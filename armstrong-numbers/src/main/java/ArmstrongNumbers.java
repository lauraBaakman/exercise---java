import java.util.stream.IntStream;

class ArmstrongNumbers {

	boolean isArmstrongNumber(int numberToCheck) {
		IntStream digits = getDigits(numberToCheck);
		long digitCount = this.getDigitCount(numberToCheck);

		double sumOfRaisedDigits = digits.mapToDouble(
				value -> Math.pow(value, digitCount)
		).sum();

		return sumOfRaisedDigits == numberToCheck;
	}

	private IntStream getDigits(int number){
		return String.valueOf(number).chars().map(Character::getNumericValue);
	}

	private long getDigitCount(int number){
		return String.valueOf(number).chars().count();
	}
}

