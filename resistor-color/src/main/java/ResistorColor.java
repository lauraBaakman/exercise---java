import java.util.Map;

class ResistorColor {

    private static Map<String, Integer> color_mapping = Map.ofEntries(
            Map.entry("black", 0),
            Map.entry("brown", 1),
            Map.entry("red", 2),
            Map.entry("orange", 3),
            Map.entry("yellow", 4),
            Map.entry("green", 5),
            Map.entry("blue", 6),
            Map.entry("violet", 7),
            Map.entry("grey", 8),
            Map.entry("white", 9)
    );

    int colorCode(String color) {
        return ResistorColor.color_mapping.get(color);
    }

    String[] colors() {
        String[] colors = new String[ResistorColor.color_mapping.size()];
        ResistorColor.color_mapping.entrySet().stream().sorted(
                Map.Entry.comparingByValue()
        ).forEachOrdered(
                entry -> colors[entry.getValue()] = entry.getKey()
        );

        return colors;
    }
}
