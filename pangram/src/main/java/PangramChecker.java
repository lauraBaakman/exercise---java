public class PangramChecker {

    private static char[] alphabet = {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
            'v', 'w', 'x', 'y', 'z'
    };

    public boolean isPangram(String input) {
        String lowerCaseInput = input.toLowerCase();
        for (char letter : alphabet) {
            if(! lowerCaseInput .contains(Character.toString(letter))){
                return false;
            }
        }
        return true;
    }

}