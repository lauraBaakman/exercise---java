import java.lang.reflect.Array;
import java.util.*;

class DnDCharacter {

    class Ability {
        private int value;

        public Ability(int value){
            this.value = value;
        }

        public Ability(Dice dice, int rollCount){
            ArrayList<Integer> rolls = dice.roll(rollCount);
            this.value = rolls.stream().mapToInt(Integer::intValue).sum() - Collections.min(rolls);
        }

        public int getValue() {
            return this.value;
        }

        public int modifier(){
            return Math.floorDiv((this.value - 10), 2);
        }
    }

    class Dice {
        private int faceCount;
        private Random randomGenerator;

        public Dice(int faceCount){
            this.faceCount = faceCount;
            this.randomGenerator = new Random();
        }

        public ArrayList<Integer> roll(int rollCount){
            ArrayList<Integer> rolls = new ArrayList<>(rollCount);
            for (int i = 0; i < rollCount; i++){
                rolls.add(this.roll());
            }
            return rolls;
        }

        public int roll(){
            return this.randomGenerator.nextInt(faceCount) + 1;
        }
    }

    private static int initialHitPoints = 10;

    private Ability strength;
    private Ability dexterity;
    private Ability constitution;
    private Ability intelligence;
    private Ability wisdom;
    private Ability charisma;

    public DnDCharacter(){
        int rollCount = 4;
        Dice dice = new Dice(6);

        this.strength = new Ability(dice, rollCount);
        this.dexterity = new Ability(dice, rollCount);
        this.constitution = new Ability(dice, rollCount);
        this.intelligence = new Ability(dice, rollCount);
        this.wisdom = new Ability(dice, rollCount);
        this.charisma = new Ability(dice, rollCount);
    }

    int modifier(int input) {
        return new Ability(input).modifier();
    }

    int getStrength() {
        return this.strength.getValue();
    }

    int getDexterity() {
        return this.dexterity.getValue();
    }

    int getConstitution() {
        return this.constitution.getValue();
    }

    int getIntelligence() {
        return this.intelligence.getValue();
    }

    int getWisdom() {
        return this.wisdom.getValue();
    }

    int getCharisma() {
        return this.charisma.getValue();
    }

    int getHitPoints() {
        return this.initialHitPoints + this.constitution.modifier();
    }
}
