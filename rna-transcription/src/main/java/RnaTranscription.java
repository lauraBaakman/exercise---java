import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;

class RnaTranscription {

    private static Map<Character, String> mapping = Map.ofEntries(
            new AbstractMap.SimpleEntry<>('G', "C"),
            new AbstractMap.SimpleEntry<>('C', "G"),
            new AbstractMap.SimpleEntry<>('T', "A"),
            new AbstractMap.SimpleEntry<>('A', "U")
    );

    String transcribe(String dnaStrand) {
        return dnaStrand.chars().mapToObj(
                character -> (char) character
        ).map(mapping::get).collect(
                Collectors.joining()
        );
    }

}