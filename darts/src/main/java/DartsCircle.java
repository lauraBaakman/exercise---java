public class DartsCircle {

    private int score;
    private double squaredRadius;

    public DartsCircle(double radius, int score) {
        this.score = score;
        this.squaredRadius = radius * radius;
    }

    public double getSquaredRadius() {
        return squaredRadius;
    }

    public int getScore() {
        return score;
    }
}
