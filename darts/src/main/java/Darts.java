class Darts {

    private static DartsCircle innerCircle = new DartsCircle(1, 10);
    private static DartsCircle middleCircle = new DartsCircle(5, 5);
    private static DartsCircle outerCircle = new DartsCircle(10, 1);

    private static int defaultScore = 0;

    private double squaredDistanceToCenter;

    Darts(double x, double y) {
        this.squaredDistanceToCenter = x * x + y * y;
    }

    int score() {
        if(this.within(innerCircle)) return innerCircle.getScore();
        if(this.within(middleCircle)) return middleCircle.getScore();
        if(this.within(outerCircle)) return outerCircle.getScore();
        return Darts.defaultScore;
    }

    private boolean within(DartsCircle circle){
        return this.squaredDistanceToCenter <= circle.getSquaredRadius();
    }
}
